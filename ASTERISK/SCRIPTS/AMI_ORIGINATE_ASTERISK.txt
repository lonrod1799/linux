#!/bin/bash

# Variables de configuración AMI
AMI_USERNAME="admin"
AMI_PASSWORD="dyalogo"
ASTERISK_HOST="127.0.0.1"  # Cambia esto si Asterisk no está en el mismo servidor
ASTERISK_PORT="5038"

# Variables de configuración de la llamada
CHANNEL="SIP/IpsRangel_SipMovil_Saliente_ID284/573156539098"
CONTEXT="tu_contexto"
EXTEN="tu_extension"
CALLER_ID_NAME="Tu Nombre"
CALLER_ID_NUMBER="tu_numero_de_llamante"

# Comando AMI para realizar el originate
AMI_COMMAND="Action: Login\r\nUsername: $AMI_USERNAME\r\nSecret: $AMI_PASSWORD\r\n\r\nAction: Originate\r\nChannel: $CHANNEL\r\nContext: $CONTEXT\r\nExten: $EXTEN\r\nPriority: 1\r\nCallerID: \"$CALLER_ID_NAME\" <$CALLER_ID_NUMBER>\r\nCallerIDName: $CALLER_ID_NAME\r\nAsync: yes\r\n\r\n"

# Enviar comando AMI a Asterisk
echo -ne "$AMI_COMMAND" | nc $ASTERISK_HOST $ASTERISK_PORT