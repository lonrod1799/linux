#!/bin/bash

# Variables de conexión a la base de datos
DB_USER="dyalogoadm"
DB_PASSWORD="dyalogoadm*bd"
DB_NAME="dyalogo_telefonia"

# Obtener la fecha actual en formato AAAA-MM-DD
FECHA=$(date +%F)

# Comando para ejecutar el procedimiento almacenado con el ID_CAMPAÑA proporcionado
function execute_procedure {
    ID_CAMPAÑA=$1
    MYSQL_COMMAND="mysql -u$DB_USER -p$DB_PASSWORD -D $DB_NAME -e \"CALL sp_llena_info_act_cam_fecha($ID_CAMPAÑA, '$FECHA');\""
    eval "$MYSQL_COMMAND"
}

# Ejecutar el procedimiento almacenado para cada ID_CAMPAÑA
execute_procedure 2
execute_procedure 23
execute_procedure 24
execute_procedure 25ActualizaACDdia.sh
