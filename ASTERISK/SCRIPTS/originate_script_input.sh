#!/bin/bash

# Variables de configuración AMI
AMI_USERNAME="admin"
AMI_PASSWORD="dyalogo"
ASTERISK_HOST="127.0.0.1"  # Cambiar esto si Asterisk no está en el mismo servidor
ASTERISK_PORT="5038"


echo "Ingresa la configuración del canal:"
read -p "Nombre de la troncal: " NOMBRE_TRONCAL
read -p "Numero: " NUMERO
read -p "contexto: " CONTEXT
read -p "extension: " EXTEN
read -p "callerid name: " CALLER_ID_NAME
read -p "callerid num: " CALLER_ID_NUMBER

CHANNEL=SIP/$NOMBRE_TRONCAL/$NUMERO

# Comando AMI para realizar el originate
AMI_COMMAND="Action: Login\r\nUsername: $AMI_USERNAME\r\nSecret: $AMI_PASSWORD\r\n\r\nAction: Originate\r\nChannel: $CHANNEL\r\nContext: $CONTEXT\r\nExten: $EXTEN\r\nPriority: 1\r\nCallerID: \"$CALLER_ID_NAME\" <$CALLER_ID_NUMBER>\r\nCallerIDName: $CALLER_ID_NAME\r\nAsync: yes\r\n\r\n"

# Enviar comando AMI a Asterisk
echo -ne "$AMI_COMMAND" | nc $ASTERISK_HOST $ASTERISK_PORT