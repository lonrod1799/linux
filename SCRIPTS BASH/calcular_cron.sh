#!/bin/bash

validar_entrada() {
    local entrada=$1
    local nombre_campo=$2
    if [[ ! "$entrada" =~ ^([0-9/*,-]+|[*])$ ]]; then
        echo "Error: $nombre_campo debe contener solo números, asteriscos (*), comas (,) y guiones (-)."
        exit 1
    fi
}


read -p "Ingresa el minuto (0-59 o * para todos los minutos): " minuto
read -p "Ingresa la hora (0-23 o * para todas las horas): " hora
read -p "Ingresa el día del mes (1-31 o * para todos los días): " dia_mes
read -p "Ingresa el mes (1-12 o * para todos los meses): " mes
read -p "Ingresa el día de la semana (0-7 o * para todos los días de la semana): " dia_semana

validar_entrada "$minuto" "Minuto"
validar_entrada "$hora" "Hora"
validar_entrada "$dia_mes" "Día del mes"
validar_entrada "$mes" "Mes"
validar_entrada "$dia_semana" "Día de la semana"

mostrar_cron() {
    local cron_configurado="$1"
    echo -e "\nCron configurado:\n$cron_configurado /ruta/script/\n"
}

if [ "$dia_mes" = "*" ]; then
    cron_configurado="$minuto $hora * $mes $dia_semana"
else
    cron_configurado="$minuto $hora $dia_mes $mes $dia_semana"
fi

mostrar_cron "$cron_configurado"