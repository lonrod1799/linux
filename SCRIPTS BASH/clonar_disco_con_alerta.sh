#!/bin/bash
nombre_servidor=$1
# Identificar discos
identificar_discos() {
    disco_sistema=$(df / | tail -1 | cut -d' ' -f1)
    disco_sistema=${disco_sistema%[0-9]}

    discos_datos=$(lsblk -nlo NAME,MOUNTPOINT | grep '/mnt/' | grep -v 'tmpfs' | cut -d' ' -f1)
}

# Obtener hostname
obtener_nombre_host() {
    nombre_host=$(hostname)
}

echo "" > /var/log/creacion_imagen.log
# Registrar tiempo
registrar_tiempo() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1: $2 segundos" >> /var/log/creacion_imagen.log
}

# Identificar discos y obtener nombre del host
identificar_discos
obtener_nombre_host

echo "Elija 's' para el sistema operativo o 'd' para los datos del disco adicional:"
read eleccion

if [ "$eleccion" = "s" ]; then
    destino="/mnt/colmedicos"
    disco_origen=$disco_sistema
elif [ "$eleccion" = "d" ]; then
    destino="/mnt/img_datos"
    # Si hay múltiples discos de datos, permite  elegir
    if [ $(echo "$discos_datos" | wc -l) -gt 1 ]; then
        echo "Múltiples discos de datos detectados. Por favor, elija uno:"
        select disco in $discos_datos; do
            disco_origen="/dev/$disco"
            break
        done
    else
        disco_origen="/dev/$discos_datos"
    fi
else
    echo "Opción no válida. Saliendo."
    exit 1
fi

# Crear la imagen
nombre_imagen="${disco_origen##*/}_${nombre_host}.img"
echo "Creando imagen..."
tiempo_inicio=$(date +%s)
echo "particion es: $disco_origen  destino es: $destino/$nombre_imagen "
sudo dd if=$disco_origen of=$destino/$nombre_imagen bs=4M
#sleep 20
tiempo_fin=$(date +%s)
duracion=$((tiempo_fin - tiempo_inicio))
registrar_tiempo "Creación de imagen" $duracion

#Envio de notificacion a telegram
BOT_TOKEN="7399161932:AAEyII5YOO1BiriCcSGuPUREam3VeuiHQpo"
CHAT_ID="-4266384129"
MESSAGE="Imagen del disco $nombre_imagen del servidor $1 finalizo creacion de imagen raw, se inicia conversión a VHD"

curl -s -X POST "https://api.telegram.org/bot$BOT_TOKEN/sendMessage" \
     -d "chat_id=-4266384129" \
     -d "text=$MESSAGE"


# Redimensionar el tamaño de la imgen raw al formato múltiplo de 1MB

echo "Redimensionando el tamaño de la imagen raw ........"
rawdisk=$destino/$nombre_imagen
MB=$((1024*1024))
size=$(qemu-img info -f raw --output json "$rawdisk" | \
gawk 'match($0, /"virtual-size": ([0-9]+),/, val) {print val[1]}')

rounded_size=$(((($size+$MB-1)/$MB)*$MB))

#echo "Rounded Size = $rounded_size"

qemu-img resize $rawdisk  $rounded_size

echo "Imagen redimensionada a $rounded_size BYTES"

# Convertir la imagen a VHD
echo "Convirtiendo imagen a VHD..."
tiempo_inicio=$(date +%s)
echo "Origen de imagen es: $destino/$nombre_imagen Destino imagen es: $destino/${nombre_imagen%.img}.vhd "
qemu-img convert -p -f raw -O vpc -o subformat=fixed $destino/$nombre_imagen $destino/${nombre_imagen%.img}.vhd
#sleep 20
tiempo_fin=$(date +%s)
duracion=$((tiempo_fin - tiempo_inicio))
registrar_tiempo "Conversión a VHD" $duracion
echo "Proceso completado de conversión de imagen. Los tiempos se han registrado en /var/log/creacion_imagen.log"

#Envio de notificacion a telegram
BOT_TOKEN="7399161932:AAEyII5YOO1BiriCcSGuPUREam3VeuiHQpo"
CHAT_ID="-4266384129"
MESSAGE="Imagen del disco ${nombre_imagen%.img}.vhd del servidor $1 finalizo conversión a VHD, se inicia transferencia a Blob Storage"

curl -s -X POST "https://api.telegram.org/bot$BOT_TOKEN/sendMessage" \
     -d "chat_id=-4266384129" \
     -d "text=$MESSAGE"


# Proceso de envio a blob Storage
local_path=$destino/${nombre_imagen%.img}.vhd

destination_url="https://sac160rp01prd001.blob.core.windows.net/imgdisk?sp=racwd&st=2024-06-20T03:15:04Z&se=2024-08-01T11:15:04Z&spr=https&sv=2022-11-02&sr=c&sig=20HXVY0FNJZf7uW8ScSh%2FfVfyevffwUNmj%2Bz3pf0PGA%3D"

tiempo_inicio=$(date +%s)
azcopy copy --recursive "$local_path" "$destination_url"
exit_status=$?

#Verificar el estado de salida

if [ $exit_status -eq 0 ]; then
  echo "La subida de archivos se realizó con éxito"
tiempo_fin=$(date +%s)
duracion=$((tiempo_fin - tiempo_inicio))
registrar_tiempo "Transferencia a Blob storage" $duracion
echo "Proceso completado de paso a blob storage. Los tiempos se han registrado en /var/log/creacion_imagen.log"

#Envio de notificación a telegram
BOT_TOKEN="7399161932:AAEyII5YOO1BiriCcSGuPUREam3VeuiHQpo"
#CHAT_ID="5003671158"
CHAT_ID="-4266384129"
MESSAGE="Imagen del disco ${nombre_imagen%.img}.vhd del servidor $1 finalizo transferencia a blob storage en $duracion segundos"

curl -s -X POST "https://api.telegram.org/bot$BOT_TOKEN/sendMessage" \
     -d "chat_id=-4266384129" \
     -d "text=$MESSAGE"

else
  echo "error --- Código de salida: $exit_status" >> /var/log/transferencia_blob.txt

BOT_TOKEN="7399161932:AAEyII5YOO1BiriCcSGuPUREam3VeuiHQpo"
#CHAT_ID="5003671158"
CHAT_ID="-4266384129"
MESSAGE="Imagen del disco ${nombre_imagen%.img}.vhd del servidor $nombre_host no se transfirio al blob storage, revisar el log..."

curl -s -X POST "https://api.telegram.org/bot$BOT_TOKEN/sendMessage" \
     -d "chat_id=-4266384129" \
     -d "text=$MESSAGE"

fi